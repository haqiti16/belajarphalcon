<?php

$tablename = "member";

try {
    $db = new PDO("mysql:dbname=$dbname;host=$host;CHARSET=utf8;COLLATE=utf8_unicode_ci", $user, $pass);
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    $db->exec("TRUNCATE TABLE $tablename;");
    $db->exec("INSERT INTO $tablename (nama, email, jeniskelamin,alamat) VALUES ('paijo', 'paijo@mail.com','L', 'jalan sudarman')");
    $db->exec("INSERT INTO $tablename (nama, email, jeniskelamin,alamat) VALUES ('tukiyem', 'tukiyem@mail.com','P', 'Jalan coba aja dulu')");
    print("Seed $tablename Table success.\n");
} catch(PDOException $e) {
    echo $e->getMessage();
}