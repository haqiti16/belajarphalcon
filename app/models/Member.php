<?php
namespace Application\Models;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;
class Member extends Model
{
    public $id;
    public $nama;
    public $email;
    public $jeniskelamin;
    public $alamat;
    public $created_at;
    public $updated_at;
    public $is_deleted;

    public function initialize() {
        $this->setSchema("belajarphalcon");
        $this->setSource("member");
      }

      public function getSource() {
        return 'member';
      }

    public function softdelete() {
		$this->is_deleted = 1;
		$this->update();
	}


}
