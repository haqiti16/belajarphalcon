<?php
/**
 * Created by PhpStorm.
 * User: gamalan
 * Date: 07/10/16
 * Time: 10:24
 */

namespace Application\Router;

use Phalcon\Mvc\Router\Group;

class MainRouter extends Group
{
    public function initialize()
    {
        $this->setPaths([
            'namespaces' => 'Application\\Controllers',
            'controller'=>'index'
        ]);

        $this->add(
            '/',
            [
                'action' => 'index'
            ]
        );
        $this->addGet(
            '/add',
            [
                'action' => 'add'
            ]
        );
        $this->addPost(
            '/add',
            [
                'action' => 'save'
            ]
        );

        $this->add(
            '/edite/{id}',
            [
                'action' => 'edite'
            ]
        );

        $this->add(
            '/delete/{id}',
            [
                'action' => 'delete'
            ]
        );
        
    }
}