{% block content %}
 <br><br>
 <div class="row">
  <div class="col-md-12">
   <form method="POST">
    <div class="form-group">
        <label>Nama member</label>
        <input type="text" name="nama" class="form-control" placeholder="Type your name" required value="{{ member.nama }}">
    </div>
    <div class="form-group">
        <label>Email members</label>
        <input type="email" name="email" class="form-control" placeholder="Type your email" required value="{{ member.email }}">
    </div>
    <div class="form-group">
        <label>Jenis Kelamin</label>
        <input type="text" name="gender" class="form-control" placeholder="Type your gender" required value="{{ member.jeniskelamin }}">
       </div>
    <div class="form-group">
        <label>Alamat Member</label>
        <textarea name="alamat" class="form-control" placeholder="Type your address" required>{{ member.alamat }}</textarea>
    </div>
    <input type="submit" value="Save"> <button type="button" onclick="window.location.href='../..'">Cancel</button>
   </form>
  </div>
 </div>
{% endblock %}