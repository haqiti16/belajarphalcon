{% block content %}
<nav class="navbar navbar-expand-lg ftco_navbar ftco-navbar-light">
    <div class="container">
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="#" class="nav-link text-uppercase">Beranda</a>
                </li>
                <li class="nav-item cta">
                    <a href="/add" class="btn btn-primary">
                        <span>Tambah member</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="row">
    <div class="col-md-12">
     <br><br>
     <table width="100%">
      <tr>
       <th>Nama</th>
       <th>Email</th>
       <th>Gender</th>
       <th>Alamat</th>
       <th>Action</th>
      </tr>
      {% for members in members %}
       <tr>
        <td>{{ members.nama }}</td>
        <td>{{ members.email }}</td>
        <td>{{ members.jeniskelamin }}</td>
        <td>{{ members.alamat }}</td>
        <td> <a href="delete/{{ members.id }}" class="btn btn-red">Delete</a> <a href="edite/{{ members.id }}" class="btn btn-primary">Edite</a>
        </td>
       </tr>
      {% endfor %}      
     </table>
    </div>
</div>
{% endblock %}  